package com.example.demo.cart;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * CartRepository
 */
public interface CartRepository extends JpaRepository<Cart, Integer> {

    List<Cart> findByUserEmail(String email);

}