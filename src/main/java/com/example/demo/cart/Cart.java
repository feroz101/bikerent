package com.example.demo.cart;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.example.demo.bike.Bike;
import com.example.demo.user.User;

/**
 * Cart
 */
@Entity
@Table
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne
    private User user;
    @ManyToMany
    private Set<Bike> bikes = new HashSet<>();

    public Cart() {
        // cart to reserve bikes
    }

    public Cart(int id, User user, Set<Bike> bikes) {
        this.id = id;
        this.user = user;
        this.bikes = bikes;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Bike> getBikes() {
        return this.bikes;
    }

    public void setBikes(Set<Bike> bikes) {
        this.bikes = bikes;
    }

}