package com.example.demo.cart;

import org.springframework.stereotype.Service;

/**
 * CartService
 */
@Service
public class CartService {

    private CartRepository cartRepository;

    public CartService(final CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    public void addBikeInCart(Cart cart) {
        cartRepository.save(cart);
    }

}