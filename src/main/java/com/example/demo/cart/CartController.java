package com.example.demo.cart;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * CartController
 */
@RestController
@RequestMapping("/api/v1/cart")
public class CartController {

    private CartService cartService;

    CartController(final CartService cartService) {
        this.cartService = cartService;
    }

    public void addToCart(@RequestBody Cart cart) {
        // add to cart
    }

    public void removeFromCart() {
        // remove from cart
    }

}