package com.example.demo.exception;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.validation.FieldError;

/**
 * ApplicationError
 */
public class ApplicationValidationError extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final Map<String, String> codes = new HashMap<>();

    public ApplicationValidationError(String msg) {
        super(msg);
    }

    public ApplicationValidationError(List<FieldError> codes) {

        for (FieldError fr : codes) {

            this.codes.put(fr.getField(), fr.getDefaultMessage());

        }

    }

    public Map<String, String> getCodes() {
        return this.codes;
    }
}
