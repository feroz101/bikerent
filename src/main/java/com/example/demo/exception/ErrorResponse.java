package com.example.demo.exception;

/**
 * ErrorResponse
 */
public class ErrorResponse {

    private String error;
    private String message;
    private String path;

    public ErrorResponse(String error, String message, String path) {
        this.error = error;
        this.message = message;
        this.path = path;
    }

    public String getError() {
        return this.error;
    }

    public String getMessage() {
        return this.message;
    }

    public String getPath() {
        return this.path;
    }

}