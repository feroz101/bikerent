package com.example.demo.exception;

import java.util.Map;

/**
 * ValidationErrorResponse
 */

public class ValidationErrorResponse {

    private Map<String, String> codes;

    public ValidationErrorResponse(Map<String, String> codes) {
        this.codes = codes;
    }

    public Map<String, String> getCode() {
        return this.codes;
    }
}