package com.example.demo.exception;

import com.example.demo.bike.exception.BikeNotFoundException;
import com.example.demo.user.exception.UserExistsException;
import com.example.demo.user.exception.UserNotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;

/**
 * GlobalExceptionHandler
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ExceptionHandlerExceptionResolver {

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleUserNotFoundException(UserNotFoundException ex, WebRequest req) {
        return new ResponseEntity<>(
                new ErrorResponse("NOT FOUND", ex.getMessage(), ((ServletWebRequest) req).getRequest().getRequestURI()),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BikeNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleBikeNotFoundException(BikeNotFoundException ex, WebRequest req) {
        return new ResponseEntity<>(
                new ErrorResponse("NOT FOUND", ex.getMessage(), ((ServletWebRequest) req).getRequest().getRequestURI()),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ApplicationValidationError.class)
    public ResponseEntity<ValidationErrorResponse> handleApplicationError(ApplicationValidationError err,
            WebRequest req) {
        return new ResponseEntity<>(new ValidationErrorResponse(err.getCodes()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserExistsException.class)
    public ResponseEntity<ErrorResponse> handleUserExistsException(UserExistsException ex, WebRequest req) {
        return new ResponseEntity<>(new ErrorResponse("USER ALLREADY EXISTS", ex.getMessage(),
                ((ServletWebRequest) req).getRequest().getRequestURI()), HttpStatus.IM_USED);
    }
    @ExceptionHandler(AppException.class)
    public ResponseEntity<ErrorResponse> handleAppException(AppException ex,WebRequest req){
        return new ResponseEntity<>(new ErrorResponse("App Exception", ex.getMessage(),
        ((ServletWebRequest) req).getRequest().getRequestURI()), HttpStatus.BAD_REQUEST);
    }
}