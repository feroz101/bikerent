package com.example.demo.security;

import com.example.demo.user.User;
import com.example.demo.user.UserRepository;
import com.example.demo.user.UserService;
import com.example.demo.user.exception.UserNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * CustomUserDetailsService
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

    private static final Logger log=LoggerFactory.getLogger(CustomUserDetailsService.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.debug("Loading User by Username");
        User user = userRepository.getUserByEmail(username).orElseThrow(()->new UserNotFoundException("User Not Found"));
        return  UserPrincipal.create(user);
    }

    public UserDetails loadUserByUserId(long id) {
        log.debug("Loading User by id");
        User user = userRepository.findById(id).orElseThrow(()->new UserNotFoundException("User Not Found"));
        return  UserPrincipal.create(user);
    }

}