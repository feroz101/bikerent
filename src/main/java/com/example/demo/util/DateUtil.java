package com.example.demo.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.example.demo.exception.AppException;

/**
 * DateUtil
 */
public class DateUtil {

    public static Date mapToDate(String strDate) {
        // converting string type date into java.util.date
        Date date = null;
        if (!strDate.isEmpty() && strDate.length() > 0) {
            try {
                date = new SimpleDateFormat("dd/mm/yyyy").parse(strDate);
            } catch (ParseException e) {
                throw new AppException(e.getMessage());
            }

        }
        return date;
    }

}
