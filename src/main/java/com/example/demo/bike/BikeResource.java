package com.example.demo.bike;

import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * BikeResource
 */
public class BikeResource extends ResourceSupport {
    private final Bike bike;

    public BikeResource(final Bike bike) {
        this.bike = bike;
        final String bikeName = bike.getName();
        add(linkTo(BikeController.class).withRel("bike"));
        add(linkTo(methodOn(BikeController.class).getBike(bikeName)).withSelfRel());

    }

    public Bike getBike() {
        return this.bike;
    }
}