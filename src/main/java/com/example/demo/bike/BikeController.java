package com.example.demo.bike;

import java.util.List;

import javax.validation.Valid;

import com.example.demo.bike.exception.BikeNotFoundException;
import com.example.demo.bike.payload.CreateBikeRequest;
import com.example.demo.exception.AppException;
import com.example.demo.exception.ApplicationValidationError;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * BikeController
 */
@RestController
@RequestMapping("/api/v1/bikes")
public class BikeController {

	private BikeService bikeService;
	private BikeMapper bikeMapper;

	public BikeController(final BikeService bikeService, final BikeMapper bikeMapper) {
		this.bikeService = bikeService;
		this.bikeMapper = bikeMapper;

	}

	@PostMapping
	@Secured("ROLE_ADMIN")
	public ResponseEntity<BikeDTO> saveBike(@Valid @RequestBody CreateBikeRequest bike, BindingResult result) {

		if (result.hasErrors()) {
			throw new ApplicationValidationError(result.getFieldErrors());
		}

		Bike newBikeForSave = bikeMapper.mapToBike(bike);
		if (this.bikeService.isBikeExists(newBikeForSave.getName())) {
			throw new AppException("Bike Is Allready Exists.");
		}
		this.bikeService.save(newBikeForSave);
		return new ResponseEntity<>(bikeMapper.mapToBikeDTO(newBikeForSave), HttpStatus.CREATED);
	}

	@GetMapping("/{name}")
	public ResponseEntity<BikeResource> getBike(@PathVariable String name) {
		return new ResponseEntity<>(
				new BikeResource(bikeService.getBike(name)
						.orElseThrow(() -> new BikeNotFoundException("Bike Not Found With Name : " + name))),
				HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<List<BikeDTO>> getBikes() {
		List<Bike> bikes = bikeService.getAllBikes();
		if (bikes.isEmpty()) {
			throw new AppException("No Bikes Found");
		}
		return ResponseEntity.ok(bikeMapper.mapToBikeDTOs(bikes));
	}

}