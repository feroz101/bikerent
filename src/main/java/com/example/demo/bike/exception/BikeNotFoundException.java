package com.example.demo.bike.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * BikeNotFoundException
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Bike Not Found")
public class BikeNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public BikeNotFoundException() {
        // bike not found
    }

    public BikeNotFoundException(String msg) {
        super(msg);
    }
}