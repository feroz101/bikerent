package com.example.demo.bike;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Bike
 */
@Entity
@Table
public class Bike {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    @Enumerated(EnumType.ORDINAL)
    private Availability availability;
    private String model;
    private float rental;
    private boolean isOpenToSell;
    @Column(name = "description")
    private String desc;
    private boolean isReserved;
    private float average;
    private String brand;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Availability getAvailability() {
        return this.availability;
    }

    public void setAvailability(Availability availability) {
        this.availability = availability;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public float getRental() {
        return this.rental;
    }

    public void setRental(float rental) {
        this.rental = rental;
    }

    public boolean isIsOpenToSell() {
        return this.isOpenToSell;
    }

    public boolean getIsOpenToSell() {
        return this.isOpenToSell;
    }

    public void setIsOpenToSell(boolean isOpenToSell) {
        this.isOpenToSell = isOpenToSell;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public boolean isIsReserved() {
        return this.isReserved;
    }

    public boolean getIsReserved() {
        return this.isReserved;
    }
 
    public void setIsReserved(boolean isReserved) {
        this.isReserved = isReserved;
    }

    public float getAverage() {
        return this.average;
    }

    public void setAverage(float average) {
        this.average = average;
    }

    public String getBrand() {
        return this.brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

}