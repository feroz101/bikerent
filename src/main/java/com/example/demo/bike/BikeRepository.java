package com.example.demo.bike;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * BikeRepository
 */
public interface BikeRepository extends JpaRepository<Bike, Integer> {

    Optional<Bike> findByNameIgnoreCase(String name);
    boolean existsByName(String name);
}