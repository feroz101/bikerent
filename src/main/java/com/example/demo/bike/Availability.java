package com.example.demo.bike;

import org.springframework.context.annotation.Scope;

@Scope("singleton")
public enum Availability {
    AVAILABLE, NOT_AVAILABLE, RESERVED
}