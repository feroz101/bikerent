package com.example.demo.bike.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * CreateBikeRequest
 */
public class CreateBikeRequest {
    @NotNull(message = "Name Should Not Null")
    @NotBlank(message = "Name Must Not Blank")
    @Pattern(regexp = "^[a-zA-Z0-9]+$")
    private String name;
    @NotNull(message = "Name Should Not Null")
    @NotBlank(message = "Name Must Not Blank")
    @Size(min = 10, message = "Please Add Description at least greate than 10 characters")
    private String desc;
    private float rental;
    private String model;
    private float average;
    private boolean isOpenToSell;
    @NotNull(message = "Brand Name Should Not Null")
    @NotBlank(message = "Brand Name Must Not Blank")
    private String brand;

    public CreateBikeRequest(String name, String desc, float rental, String model, float average, boolean isOpenToSell,
            String brand) {
        this.name = name;
        this.desc = desc;
        this.rental = rental;
        this.model = model;
        this.average = average;
        this.isOpenToSell = isOpenToSell;
        this.brand = brand;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public float getRental() {
        return this.rental;
    }

    public void setRental(float rental) {
        this.rental = rental;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public float getAverage() {
        return this.average;
    }

    public void setAverage(float average) {
        this.average = average;
    }

    public boolean getIsOpenToSell() {
        return this.isOpenToSell;
    }

    public void setIsOpenToSell(boolean isOpenToSell) {
        this.isOpenToSell = isOpenToSell;
    }

    public String getBrand() {
        return this.brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

}