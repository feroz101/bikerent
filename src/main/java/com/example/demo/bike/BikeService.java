package com.example.demo.bike;

import java.util.List;
import java.util.Optional;

import com.example.demo.bike.exception.BikeNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * BikeService
 */
@Service
public class BikeService {
    private static final Logger log = LoggerFactory.getLogger(BikeService.class);

    private BikeRepository bikeRepository;

    public BikeService(final BikeRepository bikeRepository) {
        this.bikeRepository = bikeRepository;
    }

    @Transactional
    public Bike save(Bike bike) {
    	  return bikeRepository.save(bike);
    }

    public void delete(int id) {
        bikeRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Optional<Bike> getBike(String name) {
        return bikeRepository.findByNameIgnoreCase(name);
    }

    public List<Bike> getAllBikes() {
        return bikeRepository.findAll();
    }
    
    public boolean isBikeExists(String name) {
    	return bikeRepository.existsByName(name);
    }

    public Bike getBike(int id) {
        log.debug("retriving bike ");
        return bikeRepository.findById(id).orElseThrow(() -> new BikeNotFoundException("Bike Not Found"));

    }
}