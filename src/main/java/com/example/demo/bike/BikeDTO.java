package com.example.demo.bike;

/**
 * BikeDTO
 */
public class BikeDTO {

    private String name;
    private float rent;

    public BikeDTO() {
        // ..
    }

    public BikeDTO(Bike bike) {
        this.name = bike.getName();
        this.rent = bike.getRental();
    }

    public String getName() {
        return this.name;
    }

    public float getRent() {
        return this.rent;
    }

}