package com.example.demo.bike;

import java.util.List;
import java.util.stream.Collectors;

import com.example.demo.bike.payload.CreateBikeRequest;

import org.springframework.stereotype.Service;

/**
 * BikeMapper
 */
@Service
public class BikeMapper {

    public Bike mapToBike(CreateBikeRequest bikeReq) {
        Bike bike = new Bike();
        bike.setName(bikeReq.getName());
        bike.setModel(bikeReq.getModel());
        bike.setDesc(bikeReq.getDesc());
        bike.setRental(bikeReq.getRental());
        bike.setAverage(bikeReq.getAverage());
        bike.setBrand(bikeReq.getBrand());
        return bike;
    }

    public BikeDTO mapToBikeDTO(Bike bike) {
        return new BikeDTO(bike);
    }

    public List<BikeDTO> mapToBikeDTOs(List<Bike> bikes) {
        return bikes.stream().map(bike -> mapToBikeDTO(bike)).collect(Collectors.toList());
    }
}