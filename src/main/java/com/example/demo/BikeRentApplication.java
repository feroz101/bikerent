package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class BikeRentApplication {

	protected BikeRentApplication() {
		// Application Driver Class
	}

	public static void main(String[] args) {
		SpringApplication.run(BikeRentApplication.class, args);
	}
	
}
