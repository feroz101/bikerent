package com.example.demo.authentication;

import com.example.demo.exception.AppException;
import com.example.demo.security.jwt.TokenProvider;
import com.example.demo.user.User;
import com.example.demo.user.UserMapper;
import com.example.demo.user.UserService;
import com.example.demo.user.exception.UserExistsException;
import com.example.demo.user.payload.UserRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * AuthController
 */
@RestController
@RequestMapping("/api/v1/auth")
@CrossOrigin("http://localhost:2000")
public class AuthController {

    private static final Logger log=LoggerFactory.getLogger(AuthController.class);
    @Autowired    
    private AuthenticationManager authManager;
    @Autowired
    private TokenProvider tokenProvider;
    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;


@PostMapping("/login")
 public ResponseEntity<AuthResponse> authenticateUser(@RequestBody LoginRequest request){
	
     log.debug("user authenticate request:{}",request);
       Authentication authentication=authManager
       .authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
      SecurityContextHolder.getContext().setAuthentication(authentication);
      Authentication authenticated=SecurityContextHolder.getContext().getAuthentication();
     
      if(!authenticated.isAuthenticated()){
        log.error("Failed To Authenticate User");
        throw new AppException("Not Able Authenticate User");
      }

      String token=tokenProvider.createToken(authentication);
 return ResponseEntity.ok(new AuthResponse(token));
 }

@PostMapping("/register")
public ResponseEntity<Object> registerUser(@RequestBody UserRequest userRequest){
  User user = userMapper.mapToUser(userRequest);
  if (userService.checkUserPresent(user.getEmail())) {
      throw new UserExistsException("Email Allredy Used");
  }
  userService.save(user);
  return new ResponseEntity<>("User Created with email:" + user.getEmail(), HttpStatus.CREATED);

}


}