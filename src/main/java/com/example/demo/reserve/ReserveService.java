package com.example.demo.reserve;

import com.example.demo.bike.Bike;
import com.example.demo.bike.BikeService;
import com.example.demo.reserve.payloads.ReservationRequest;
import com.example.demo.user.User;
import com.example.demo.user.UserService;
import com.example.demo.util.DateUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * ReserveService
 */
@Service
public class ReserveService {

    private static final Logger log = LoggerFactory.getLogger(ReserveService.class);

    private ReserveRepository reserveRepository;
    private BikeService bikeService;

    public ReserveService(final ReserveRepository reserveRepository, final BikeService bikeService) {
        this.reserveRepository = reserveRepository;
        this.bikeService = bikeService;
    }

    @Transactional
    public void reserve(final ReservationRequest resReq,User user) {
        log.debug("reserve the bike");

        Bike bikeToReserve = bikeService.getBike(resReq.getBikeId());
        bikeToReserve.setIsReserved(true);
        Reserve reserve = new Reserve(DateUtil.mapToDate(resReq.getStartDate()),
                DateUtil.mapToDate(resReq.getEndDate()), user, bikeToReserve);

        reserveRepository.save(reserve);

    }
}
