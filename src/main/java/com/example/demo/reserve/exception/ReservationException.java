package com.example.demo.reserve.exception;

/**
 * ReservationException
 */
public class ReservationException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    // handle any reservation exception for now
}