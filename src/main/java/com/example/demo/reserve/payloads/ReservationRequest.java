package com.example.demo.reserve.payloads;

/**
 * ReservationRequest
 */
public class ReservationRequest {
    private String startDate;
    private String endDate;
    private int bikeId;

    public ReservationRequest(String startDate, String endDate, int bikeId) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.bikeId = bikeId;
    }

    public String getStartDate() {
        return this.startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return this.endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getBikeId() {
        return this.bikeId;
    }

    public void setBikeId(int bikeId) {
        this.bikeId = bikeId;
    }

}