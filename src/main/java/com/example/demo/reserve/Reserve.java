package com.example.demo.reserve;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.example.demo.bike.Bike;
import com.example.demo.user.User;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * Reserve
 */
@Entity
@Table
public class Reserve {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @DateTimeFormat(pattern = "dd/mm/yyyy")
    private Date startDate;
    @DateTimeFormat(pattern = "dd/mm/yyyy")
    private Date endDate;
    @OneToOne
    private User user;
    @OneToOne(cascade = CascadeType.ALL)
    private Bike bike;

    public Reserve() {
        // reserve bike
    }

    public Reserve(Date startDate, Date endDate, User user, Bike bike) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.user = user;
        this.bike = bike;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return this.endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Bike getBike() {
        return this.bike;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

}