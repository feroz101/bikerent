package com.example.demo.reserve;

import com.example.demo.exception.AppException;
import com.example.demo.reserve.payloads.ReservationRequest;
import com.example.demo.security.CurrentUser;
import com.example.demo.security.UserPrincipal;
import com.example.demo.user.UserService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ReserveController
 */
@RestController
@RequestMapping("/api/v1/reservations")
public class ReserveController {

    private ReserveService reserveService;
    private UserService userService;
    public ReserveController(ReserveService reserveService,UserService userService) {
        this.reserveService = reserveService;
        this.userService=userService;
    }

    @PostMapping
    public ResponseEntity<Object> reserveBike(@RequestBody ReservationRequest resReq,@CurrentUser UserPrincipal userPrincipal){
        reserveService.reserve(resReq,userService.getUserByEmail(userPrincipal.getUsername()));
        return new ResponseEntity<>(resReq, HttpStatus.CREATED);
    }

}