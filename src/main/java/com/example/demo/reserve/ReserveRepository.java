package com.example.demo.reserve;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * ReserveRepository
 */
public interface ReserveRepository extends JpaRepository<Reserve, Integer> {

}