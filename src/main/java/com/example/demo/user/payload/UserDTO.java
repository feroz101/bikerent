package com.example.demo.user.payload;

import com.example.demo.user.User;

/**
 * UserDTO
 */
public class UserDTO {
    private String email;
    private String name;

    public UserDTO() {

        // UserDTO pojo for data transfer between client to database

    }  

    public UserDTO(User user) {
        this.email = user.getEmail();
        this.name = user.getFirstName() + " " + user.getLastName();

    }

    public String getEmail() {
        return this.email;
    }

    public String getName() {
        return this.name;
    }

}