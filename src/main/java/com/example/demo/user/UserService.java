package com.example.demo.user;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.example.demo.user.exception.UserNotFoundException;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * UserService
 */
@Service
public class UserService {
    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    public UserService(final UserRepository userRepository,final PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder=passwordEncoder;
    }

    public Optional<User> save(final User user) {
        String key = UUID.randomUUID().toString();
        user.setActivationKey(key);
        user.setCreated(LocalDate.now());
        String encodedPass=passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPass);
        user.addRole(new Role("ROLE_USER"));
        return Optional.of(userRepository.save(user));
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public User getUserById(long id) {
        return userRepository.findById(id).orElseThrow(()->new UserNotFoundException("User Not Found with id:"+id));
    }

    public User getUserByEmail(String email) {
        log.debug("Finding User With Email:{}", email);
        return userRepository.getUserByEmail(email).orElseThrow(()->new UserNotFoundException("User Not Found With Email: "+email));
    }

    public void deleteUser(long id) {
        log.debug("Deleting User With Id:{}", id);
        userRepository.deleteById(id);
    }

    public boolean checkUserPresent(String email) {
        return userRepository.existsByEmail(email);
    }

    // retrive user who want to reserve a bike
    public User getUser(long id) {
        log.debug("retrive user");
        return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException("User Not Found"));
    }
}