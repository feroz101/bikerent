package com.example.demo.user;

import com.example.demo.user.payload.UserRequest;
import com.example.demo.user.payload.UserDTO;

import org.springframework.stereotype.Service;

/**
 * UserMapper
 */
@Service
public class UserMapper {

    public User mapToUser(UserRequest userRequest) {
        return new User(userRequest.getFirstName(), userRequest.getLastName(), userRequest.getEmail(),
                userRequest.getPassword());
    }

    public UserDTO mapToUserDTO(User user) {
        return new UserDTO(user);
    }

}