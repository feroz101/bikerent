
package com.example.demo.user;

import com.example.demo.security.CurrentUser;
import com.example.demo.security.UserPrincipal;
import com.example.demo.user.exception.UserNotFoundException;
import com.example.demo.user.payload.UserDTO;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * UserController
 */
@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    private UserService userService;
    private UserMapper userMapper;

    public UserController(final UserService userService, final UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @GetMapping("/{email}")
    public ResponseEntity<Object> getUser(@PathVariable String email) {
        User dbUser = userService.getUserByEmail(email);
        UserDTO userDTO = userMapper.mapToUserDTO(dbUser);
        return ResponseEntity.ok(userDTO);
    }

    @GetMapping
    public ResponseEntity<Object> currentUser(@CurrentUser UserPrincipal user) {
        return ResponseEntity.ok(user);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable int id) {
        userService.deleteUser(id);
        return ResponseEntity.ok().build();
    }

}