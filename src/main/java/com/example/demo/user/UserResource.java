package com.example.demo.user;

import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.example.demo.bike.BikeController;
import com.example.demo.user.payload.UserDTO;
/**
 * UserResource
 */
public class UserResource extends ResourceSupport {

    public UserResource(final UserDTO user) {
        final String email = user.getEmail();
        add(linkTo(UserController.class).withRel("user"));
        add(linkTo(methodOn(UserController.class).getUser(email)).withSelfRel());
        add(linkTo(methodOn(BikeController.class).getBikes()).withRel("bikes"));
    }

}
