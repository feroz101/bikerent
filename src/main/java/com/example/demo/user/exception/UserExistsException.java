package com.example.demo.user.exception;

/**
 * UserExistsException
 */
public class UserExistsException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public UserExistsException() {

    }

    public UserExistsException(String msg) {
        super(msg);
    }

}